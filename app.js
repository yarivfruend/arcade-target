function run() {
	let hitSound = new Audio("sound/hit.ogg");
	document.addEventListener("mousedown", () => {
		document.documentElement.classList.add("focus");
		hitSound.pause();
		hitSound.currentTime = 0;
		hitSound.play();
	});

	document.documentElement.addEventListener("animationend", function(e) {
		if (e.animationName === "focus") {
			document.documentElement.classList.remove("focus");
		}
	});
}

window.addEventListener("load", run);
